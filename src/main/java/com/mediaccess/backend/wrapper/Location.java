package com.mediaccess.backend.wrapper;

/**
 * Created by robinson on 3/23/16.
 */
public class Location {
    private String name;

    public Location(String name) {
        this.name =  name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals (Object obj) {
        if(obj instanceof Location) {
            Location other = (Location)obj;
            if(other.name.equalsIgnoreCase(this.name)) {
                return true;
            }
        }
        return false;
    }
}
