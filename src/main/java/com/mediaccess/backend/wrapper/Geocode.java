package com.mediaccess.backend.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by robinson on 3/23/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Geocode {
    private Geometry geometry;

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Geometry getGeometry () {
        return this.geometry;
    }
}
