package com.mediaccess.backend.wrapper;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by robinson on 3/23/16.
 */
public class Message {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String success = null;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String error = null;

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getSuccess() {
        return success;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
