package com.mediaccess.backend.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mediaccess.backend.model.Coordinate;

/**
 * Created by robinson on 3/23/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Geometry {
    private Coordinate location;

    public Coordinate getLocation() {
        return location;
    }

    public void setLocation(Coordinate location) {
        this.location = location;
    }
}
