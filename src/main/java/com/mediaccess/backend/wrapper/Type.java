package com.mediaccess.backend.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by robinson on 3/23/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Type {
    private String type;

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }
}
