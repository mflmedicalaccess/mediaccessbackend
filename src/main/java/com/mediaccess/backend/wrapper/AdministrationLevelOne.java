package com.mediaccess.backend.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by robinson on 3/23/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AdministrationLevelOne {
    private List<AddressComponent> address_components;

    public void setAddress_component(List<AddressComponent> address_components) {
        this.address_components = address_components;
    }

    public List<AddressComponent> getAddress_component () {
        return this.address_components;
    }
}
