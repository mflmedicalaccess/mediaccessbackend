package com.mediaccess.backend.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by robinson on 3/23/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AdministrationLevelOneList {
    private List<AdministrationLevelOne> results;

    public void setResults(List<AdministrationLevelOne> results) {
        this.results = results;
    }

    public List<AdministrationLevelOne> getResults() {
        return results;
    }
}
