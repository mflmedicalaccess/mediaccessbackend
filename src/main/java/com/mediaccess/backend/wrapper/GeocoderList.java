package com.mediaccess.backend.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by robinson on 3/23/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeocoderList {
    private List<Geocode> results;

    public void setResults(List<Geocode> results) {
        this.results = results;
    }

    public List<Geocode> getResults() {
        return results;
    }
}
