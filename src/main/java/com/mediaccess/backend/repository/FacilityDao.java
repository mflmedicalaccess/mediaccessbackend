package com.mediaccess.backend.repository;

import com.mediaccess.backend.model.Facility;
import com.mediaccess.backend.wrapper.Location;

import java.util.List;

/**
 * Created by robinson on 3/23/16.
 */
public interface FacilityDao {
    List<Facility> getFacilities ();
    List<Facility> getFacilities (String subcounty);
    List<Location> getCounties();
    List<Location> getSubCounties(String county);
}
