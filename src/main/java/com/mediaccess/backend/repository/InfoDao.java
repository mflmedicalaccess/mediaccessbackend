package com.mediaccess.backend.repository;


import com.mediaccess.backend.wrapper.Location;


import java.util.List;

/**
 * Created by robinson on 3/23/16.
 */
public interface InfoDao {
    List<Location> getSubCounties(String county);
}
