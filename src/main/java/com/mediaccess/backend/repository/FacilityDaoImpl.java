package com.mediaccess.backend.repository;

import com.mediaccess.backend.model.Facility;
import com.mediaccess.backend.wrapper.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;


/**
 * Created by robinson on 3/23/16.
 */

@Repository
public class FacilityDaoImpl implements FacilityDao {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Facility> getFacilities () {
        TypedQuery<Facility> query = entityManager.createQuery("SELECT f FROM Facility f",Facility.class);
        List<Facility> result = query.getResultList();
        if(result.isEmpty()) {
            return null;
        }

        return result;
    }

    public List<Facility> getFacilities (String subcounty){
        TypedQuery<Facility> query = entityManager.createQuery("SELECT f FROM Facility as f ,Info as i where f.code = i.code AND i.constituency = :subcounty",Facility.class);
        query.setParameter("subcounty",subcounty.toUpperCase());
        List<Facility> result = query.getResultList();
        if(result.isEmpty()) {
            return null;
        }

        return result;
    }

    public List<Location> getCounties () {
        TypedQuery<Location> query = entityManager.createQuery("SELECT DISTINCT new com.mediaccess.backend.wrapper.Location(f.countyName) FROM Facility f ",Location.class);
        List<Location> result = query.getResultList();
        if(result.isEmpty()) {
            return null;
        }

        return result;
    }


    public List<Location> getSubCounties(String county) {
        county = county.toUpperCase();
        TypedQuery<Location> query = entityManager.createQuery("SELECT new com.mediaccess.backend.wrapper.Location(f.subCountyName) FROM Facility f  WHERE f.countyName = :county",Location.class);
        query.setParameter("county",county.toUpperCase());
        List<Location> result = query.getResultList();
        if(result.isEmpty()) {
            return null;
        }

        return result;
    }
}
