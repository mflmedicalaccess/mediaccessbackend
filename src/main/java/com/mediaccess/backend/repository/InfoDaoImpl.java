package com.mediaccess.backend.repository;

import com.mediaccess.backend.wrapper.Location;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;


/**
 * Created by robinson on 3/23/16.
 */

@Repository
public class InfoDaoImpl implements InfoDao {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Location> getSubCounties(String county) {
        county = county.toUpperCase();
        TypedQuery<Location> query = entityManager.createQuery("SELECT DISTINCT new com.mediaccess.backend.wrapper.Location(i.constituency) FROM Info i  WHERE i.county = :county",Location.class);
        query.setParameter("county",county);
        List<Location> result = query.getResultList();
        if(result.isEmpty()) {
            return null;
        }

        return result;
    }
}
