package com.mediaccess.backend.model;

/**
 * Created by robinson on 3/23/16.
 */
public class Coordinate {
    private Double lat;
    private Double lng;

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLat() {
        return lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLng() {
        return lng;
    }
}
