package com.mediaccess.backend.model;


import javax.persistence.*;

/**
 * Created by robinson on 3/23/16.
 */

@Entity
@Table(name="facilities")
public class Facility {
    private String id;
    private Integer code;
    private String name;
    private String countyName;
    private String subCountyName;
    private String facilityTypeName;
    private String regulatoryStatusName;
    private String operationStatusName;
    private Integer numberOfBeds;
    private Integer numberOfCots;
    private Boolean openWholeDay;
    private Boolean openPublicHolidays;
    private Boolean openNormalDay;
    private Boolean openWeekends;
    private Boolean openLateNight;
    private Boolean approved;
    private Integer averageRating;
    private String kephLevelName;

    public void setId (String id) {
        this.id = id;
    }

    @Id
    public String getId () {
        return id;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getName () {
        return name;
    }

    public void setCountyName (String countyName) {
        this.countyName = countyName;
    }

    @Column(name="county")
    public String getCountyName () {
        return countyName;
    }

    public void setSubCountyName (String subCountyName) {
        this.subCountyName = subCountyName;
    }

    @Column(name="sub_county_name")
    public String getSubCountyName () {
        return subCountyName;
    }

    public void setFacilityTypeName (String facilityTypeName) {
        this.facilityTypeName = facilityTypeName;
    }

    @Column(name="facility_type_name")
    public String getFacilityTypeName () {
        return facilityTypeName;
    }

    public void setRegulatoryStatusName (String regulatoryStatusName) {
        this.regulatoryStatusName = regulatoryStatusName;
    }

    @Column(name="regulatory_status_name")
    public String getRegulatoryStatusName () {
        return regulatoryStatusName;
    }

    public void setOperationStatusName (String operationStatusName) {
        this.operationStatusName = operationStatusName;
    }

    @Column(name="operation_status_name")
    public String getOperationStatusName () {
        return operationStatusName;
    }

    public void setNumberOfBeds (Integer numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    @Column(name="number_of_beds")
    public Integer getNumberOfBeds () {
        return numberOfBeds;
    }

    public void setNumberOfCots (Integer numberOfCots) {
        this.numberOfCots = numberOfCots;
    }

    @Column(name="number_of_cots")
    public Integer getNumberOfCots () {
        return numberOfCots;
    }

    public void setOpenWholeDay (Boolean openWholeDay) {
        this.openWholeDay = openWholeDay;
    }

    @Column(name="open_whole_day")
    public Boolean getOpenWholeDay () {
        return openWholeDay;
    }

    public void setOpenPublicHolidays (Boolean openPublicHolidays) {
        this.openPublicHolidays = openPublicHolidays;
    }

    @Column(name="open_public_holidays")
    public Boolean getOpenPublicHolidays () {
        return openPublicHolidays;
    }

    public void setOpenNormalDay (Boolean openNormalDay) {
        this.openNormalDay = openNormalDay;
    }

    @Column(name="open_normal_day")
    public Boolean getOpenNormalDay () {
        return openNormalDay;
    }

    public void setOpenWeekends (Boolean openWeekends) {
        this.openWeekends = openWeekends;
    }

    @Column(name="open_weekends")
    public Boolean getOpenWeekends () {
        return openWeekends;
    }

    public void setOpenLateNight (Boolean openLateNight) {
        this.openLateNight = openLateNight;
    }

    @Column(name="open_late_night")
    public Boolean getOpenLateNight () {
        return openLateNight;
    }

    public void setApproved (Boolean approved) {
        this.approved = approved;
    }

    @Column(name="approved")
    public Boolean getApproved () {
        return approved;
    }

    public void setAverageRating(Integer averageRating) {
        this.averageRating = averageRating;
    }

    @Column(name="average_rating")
    public Integer getAverageRating() {
        return averageRating;
    }

    public void setKephLevelName(String keph_level_name) {
        this.kephLevelName = kephLevelName;
    }

    @Column(name="keph_level_name")
    public String getKephLevelName() {
        return kephLevelName;
    }
}
