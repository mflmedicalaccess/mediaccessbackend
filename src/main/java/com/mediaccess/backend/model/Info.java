package com.mediaccess.backend.model;

import javax.persistence.*;
/**
 * Created by robinson on 3/24/16.
 */

@Entity
@Table(name="more_info")
public class Info {
    private Integer code;
    private String constituency;
    private String county;

    public void setCode(Integer code) {
        this.code = code;
    }

    @Id
    public Integer getCode() {
        return this.code;
    }

    public void setconstituency(String constituency) {
        this.constituency = constituency;
    }

    public String getconstituency() {
        return this.constituency;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCounty() {
        return this.county;
    }
}
