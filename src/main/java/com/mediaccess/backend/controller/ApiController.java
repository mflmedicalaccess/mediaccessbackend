package com.mediaccess.backend.controller;

import com.mediaccess.backend.model.Facility;
import com.mediaccess.backend.repository.FacilityDao;
import com.mediaccess.backend.repository.InfoDao;
import com.mediaccess.backend.wrapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by robinson on 3/23/16.
 */

@RestController
public class ApiController {
    @Autowired
    private FacilityDao facilityDao;
    @Autowired
    private InfoDao infoDao;

    @RequestMapping("/")
    public ResponseEntity<Message> test() {
        Message msg = new Message();
        msg.setSuccess("successfully connected : services available are /facilities to list all facilities , /locations/county to get all counties ,/locations/{county}/subcounty to get a subcounty of a particular county  ");
        return new ResponseEntity<Message>(msg, HttpStatus.OK);
    }

    @RequestMapping("/facilities")
    public List<Facility> testFacilities () {
        return facilityDao.getFacilities();
    }
    @RequestMapping("/facilities/{subcounty}/")
    public List<Facility> testFacilities (@PathVariable String subcounty) {
        return facilityDao.getFacilities(subcounty);
    }

    @RequestMapping("/geocodes/{county}/")
    public ResponseEntity<GeocoderList> countyGeocodes (@PathVariable String county) {
        RestTemplate restTemplate = new RestTemplate();
        GeocoderList geo = restTemplate.getForObject("https://maps.googleapis.com/maps/api/geocode/json?address="+county+"&types=location&key=AIzaSyCZCrdEkdEpUqOlhXxWtNscWxW_Si6uMw8",GeocoderList.class);
        return new ResponseEntity<GeocoderList>(geo,HttpStatus.OK);
    }

    @RequestMapping("/locations/county")
    public List<Location> counties () {
        return facilityDao.getCounties();
    }

    @RequestMapping("/locations/{county}/subcounty")
    public List<Location> subcounties (@PathVariable String county) {
        return infoDao.getSubCounties(county);
    }

//    @RequestMapping("/facilities/{latitude}/{longitude}/")
//    public List<Facility> facilities (@PathVariable Double latitude,@PathVariable Double longitude) {
//        return facilityDao.getFacilities(latitude, longitude);
//    }
}
